
<!doctype html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Companies a Industrial Category Bootstrap Responsive Website Template | Home :: W3layouts</title>
  <!-- web fonts -->
  <link href="//fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900&display=swap" rel="stylesheet">
  <link href="//fonts.googleapis.com/css?family=Hind&display=swap" rel="stylesheet">
  <!-- //web fonts -->
  <!-- Template CSS -->
  <link rel="stylesheet" href="assets/css/style-starter.css">
  <link rel="stylesheet" href="assets/css/font-awesome.css">
  <link rel="stylesheet" href="assets/css/ionicons.min.css">
  <link rel="stylesheet" href="assets/css/style.css">
</head>
<body>

  <section class="w3l-bootstrap-header">
    <nav class="navbar navbar-expand-lg navbar-light py-lg-3 py-2">
      <div class="container">
        <!-- <a class="navbar-brand" href="index.php"><span class="fa fa-home"></span> Maison-Finie</a> -->
        <!-- if logo is image enable this -->  
        <a class="navbar-brand" href="#index.html">
          <img src="assets/images/logo.png" alt="Your logo" title="Your logo" style="height:50px; width: 90px; " /> <span style="color: #4682b4;">Maison </span> <span style="color:#cd5c5c;">Finie</span>
        </a> 
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon fa fa-bars"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mx-auto">
          <li class="nav-item">
            <a class="nav-link nav_onglet	" href="index.php">Acceuil</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#Apropos">a propos</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#services">Services</a>
          </li>
          
          <li class="nav-item">
            <a class="nav-link" href="#realisations">Realisation</a>
          </li>
          <!-- <li class="nav-item">
            <a class="nav-link" href="#">Actualites</a>
          </li> -->
          <li class="nav-item">
            <a class="nav-link" href="contact.php">Contact</a>
          </li>
          <li class="nav-item">
            <a class="btn btn-secondary" href="devisForm.php">Demander un devis</a>
          </li>
        </ul>
<!--         <form action="#" class="form-inline position-relative my-2 my-lg-0">
          <input class="form-control search" type="search" placeholder="reccherche" aria-label="Search" required="">
          <button class="btn btn-search position-absolute" type="submit"><span class="fa fa-search" aria-hidden="true"></span></button>
        </form> -->
      </div>
    </div>
  </nav>
</section>
<section class="w3l-main-slider" id="home">
  <!-- main-slider -->
  <div class="companies20-content">
    
    <div class="owl-one owl-carousel owl-theme">
      <div class="item">
        <li>
          <div class="slider-info banner-view bg bg2" data-selector=".bg.bg2">
            <div class="banner-info">
              <div class="container">
                <div class="banner-info-bg mr-auto">
                  <h5> We are industry  Factory solutions</h5>
                  <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quae, velit recusandae eum necessitatibus blanditiis porro cum</p>
                  <button class="btn btn-secondary btn-theme2 mt-md-5 mt-4" id="modalBtn" data-toggle="modal" data-target="#ModalDevis"> Nos services</button>
                  <!-- Modal -->
                  <div class="modal fade bd-example-modal-lg" id="ModalDevis" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="z-index: 99 !important">
                    <div class="modal-dialog modal-dialog-right" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <span class="modal-title" id="exampleModalLongTitle">Demander un devis</span>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body">
                          <div >
                            <div class="row">
                              
                              <div class=" col-md-12 form_section">
                                <span>Remplissez ce formulaire , envoyez le et nous vous contacterons dans les brefs delais</span>
                                <form method = 'post' action="sendMail.php">
                                  <div class="mt-5">
                                    <input type="email" name = 'email' class="form-control" placeholder="email">
                                  </div>
                                  <div class="my-3">
                                    <input type="text" name='phone' class="form-control" placeholder="telephone">  
                                  </div>
                                  <div class="my-3">
                                    <textarea name="message" id="" cols="30" placeholder="exprimez votre besoin" rows="10"></textarea>
                                    
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                                    <button type="submit" class =' btn btn-primary'>soumettre</button>
                                  </div>
                                </form>

                              </div>

                            </div>
                            

                          </div>
                        </div>
                        
                      </div>
                    </div>
                  </div>
                  <!-- <a class="btn btn-secondary btn-theme2 mt-md-5 mt-4" href="#services"> Nos services</a> -->
                </div>
              </div>
            </div>
          </div>
        </li>
      </div>
      <div class="item">
        <li>
          <div class="slider-info  banner-view banner-top1 bg bg2" data-selector=".bg.bg2">
            <div class="banner-info">
              <div class="container">
                <div class="banner-info-bg mr-auto">
                  <h5>Fast and Reliable Electrical services</h5>
                  <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quae, velit recusandae eum necessitatibus blanditiis porro cum</p>
                  <a class="btn btn-secondary btn-theme2 mt-md-5 mt-4" href="contact.php"> Contacts</a>
                </div>
              </div>
            </div>
          </div>
        </li>
      </div>
      <div class="item">
        <li>
          <div class="slider-info banner-view banner-top2 bg bg2" data-selector=".bg.bg2">
            <div class="banner-info">
              <div class="container">
                <div class="banner-info-bg mr-auto">
                  <h5>A company involved in service, maintenance</h5>
                  <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quae, velit recusandae eum necessitatibus blanditiis porro cum</p>
                  <a class="btn btn-secondary btn-theme2 mt-md-5 mt-4" href="#Apropos"> A propos</a>
                </div>
              </div>
            </div>
          </div>
        </li>
      </div>
      <div class="item">
        <li>
          <div class="slider-info banner-view banner-top3 bg bg2" data-selector=".bg.bg2">
            <div class="banner-info">
              <div class="container">
                <div class="banner-info-bg mr-auto">
                  <h5>We're thriving and building better products</h5>
                  <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quae, velit recusandae eum necessitatibus blanditiis porro cum</p>
                  <a class="btn btn-secondary btn-theme2 mt-md-5 mt-4" href="#services">Read More</a>
                </div>
              </div>
            </div>
          </div>
        </li>
      </div>
    </div>
  </div>

</div>


<script src="assets/js/owl.carousel.js"></script>
<!-- script for -->
<script>
  $(document).ready(function () {
    $('.owl-one').owlCarophp({
      loop: true,
      margin: 0,
      nav: false,
      responsiveClass: true,
      autoplay: false,
      autoplayTimeout: 5000,
      autoplaySpeed: 1000,
      autoplayHoverPause: false,
      responsive: {
        0: {
          items: 1,
          nav: false
        },
        480: {
          items: 1,
          nav: false
        },
        667: {
          items: 1,
          nav: true
        },
        1000: {
          items: 1,
          nav: true
        }
      }
    })
  })
</script>
<!-- //script -->
<!-- /main-slider -->
</section>
<section class="w3l-features-1">
	<!-- /features -->
  <div class="features py-5">
    <div class="container">
     
     <div class="fea-gd-vv row ">	
      <div class="col-lg-3 col-sm-6">	
        <div class="float-lt feature-gd">	
          <div class="icon"> <span class="fa fa-microchip" aria-hidden="true"></span></div>
          <div class="icon-info">
            <h5>Finitions</h5>
            <p class="mt-2">Lorem ipsum dolor sit amet, consectetur </p>
          </div>
        </div>
      </div>	
      <div class="col-lg-3 col-sm-6 mt-sm-0 mt-4">	
       <div class="float-mid feature-gd">	
        <div class="icon"> <span class="fa fa-flask" aria-hidden="true"></span></div>
        <div class="icon-info">
          <h5>Construction</h5>
          <p class="mt-2">Lorem ipsum dolor sit amet, consectetur </p>
        </div>
      </div>
    </div> 
    <div class="col-lg-3 col-sm-6 mt-lg-0 mt-4">	
     <div class="float-rt feature-gd">
      <div class="icon"> <span class="fa fa-fire" aria-hidden="true"></span></div>
      <div class="icon-info">
        <h5>Terrassement</h5>
        <p class="mt-2">Lorem ipsum dolor sit amet, consectetur </p>
      </div>
    </div>
  </div>	 
  <div class="col-lg-3 col-sm-6 mt-lg-0 mt-4">	
    <div class="float-lt feature-gd">	
      <div class="icon"> <span class="fa fa-cog" aria-hidden="true"></span></div>
      <div class="icon-info">
        <h5>renovations</h5>
        <p class="mt-2">Lorem ipsum dolor sit amet, consectetur </p>
      </div>
    </div>	
  </div>	
</div>  
</div>
</div>
<!-- //features -->
</section >
<!--  About section -->
<span id="Apropos"></span>
<div class="w3l-about1 py-5" id="about">
  <div class="container py-lg-3">
   
    <div class="aboutgrids row">
      <div class="col-lg-6 aboutgrid2">
        <img src="assets/images/g221.jpg" alt="about image" class="img-fluid"  />
      </div>
      <div class="col-lg-6 aboutgrid1 mt-lg-0 mt-4 pl-lg-5">
        <h4>Comment pouvons nous vous aider ?</h4>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quae, velit recusandae eum necessitatibus blanditiis porro cum, facere qui impedit dolor doloribus quis reiciendis ullam repellendus.Lorem ipsum dolor sit amet consectetur adipisicing elit. Quae, velit recusandae eum </p>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quae, velit recusandae eum necessitatibus blanditiis porro cum, facere qui impedit dolor doloribus quis reiciendis ullam repellendus.</p>

        <!-- <a class="btn btn-secondary btn-theme2" href="#Apropos"> A propos</a> -->
      </div>
    </div>
  </div>
</div>
<!-- content-with-photo4 block -->
<section class="w3l-content-with-photo-4" id="about">
  <div id="content-with-photo4-block" class="pb-5"> 
    <div class="container py-md-3">
      <div class="cwp4-two row">
       
        <div class="cwp4-text col-lg-6">
          <h3>Notre Reseau</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicingelit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.
          </p>
          
          <ul class="cont-4">
            <li><span class="fa fa-check"></span>Lorem ipsum dolor sit amet,</li>
            <li><span class="fa fa-check"></span>Lorem ipsum dolor sit amet,</li>
            <li><span class="fa fa-check"></span>Lorem ipsum dolor sit amet,</li>
            <li><span class="fa fa-check"></span>Lorem ipsum dolor sit amet,</li>
          </ul>
        </div>
        <div class="cwp4-image col-lg-6 pl-lg-5 mt-lg-0 mt-5">
          <img src="assets/images/g81.JPEG" class="img-fluid" alt="" />
        </div>
      </div>
    </div>
  </div>
</section>
<!-- content-with-photo4 block -->
<!--  //About section -->
<section class="w3l-services2" id="stats">
 <div class="features-with-17_sur py-5">
   <div class="container py-md-3">
     
     <div class="row pt-lg-5 mt-lg-3">
      <div class="col-lg-4 features-with-17-left_sur">
       <h4 class="lft-head">We're thriving and building better products</h4>
       <p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla mollis dapibus nunc, ut rhoncus turpis sodales quis.</p>
     </div>
     <div class="col-lg-8 my-lg-0 my-5 align-self-center features-with-17-right_sur">
       <div class="features-with-17-right-tp_sur text-center">
        <div class="features-with-17-left1">
         <span class="fa fa-user s4"></span>		
       </div>
       <div class="features-with-17-left2">
         <h6>1100</h6>
         <p>HAPPY CLIENT</p>		
       </div>
     </div>
     <div class="features-with-17-right-tp_sur text-center">
      <div class="features-with-17-left1">
        <span class="fa fa-handshake-o s5"></span>		
      </div>
      <div class="features-with-17-left2">
        <h6 class="numscroller numscroller-big-bottom roller-title-number-6 scrollzip isShown" data-slno="6" data-min="0" data-max="16" data-delay=".1" data-increment="1">1208</h6>
        <p>WORKERS HAND</p>			
      </div>
    </div>
    <div class="features-with-17-right-tp_sur text-center">
      <div class="features-with-17-left1">
       <span class="fa fa-lock s3"></span>		
     </div>
     <div class="features-with-17-left2">
      <h6>1250</h6>
      <p>ACTIVE EXPERTS</p>	
    </div>
  </div>
  
  
  
</div>
</div>
</div>
</div>
</section>
<span id="services"></span>
<section class="w3l-features-8">
 <!-- /features -->
 <div class="features py-5" id="features">
   <div class="container py-md-3">
    
    <div class="heading text-center mx-auto">
     <h3 class="head">Nos services</h3>
     <p class="my-3 head"> Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;
       Nulla mollis dapibus nunc, ut rhoncus
     turpis sodales quis. Integer sit amet mattis quam.</p>
     
   </div>
   <div class="fea-gd-vv text-center row mt-5 pt-3">
    
     <div class="col-lg-4 col-md-6">	
      <div class="float-top">	
       <a href="services.php"><img src="assets/images/g11.jpg" class="img-responsive" height="200px" alt=""></a>
       <div class="float-lt feature-gd">	
         <h3><a href="services.php">Terrassement </a> </h3>
         <p> Consectetur adipisicingelit, sed do eiusmod tempor incididunt ut labore et.dolor sit amet </p>
         <!-- <a class="btn btn-secondary btn-theme4 mt-4" href="services.php"> plus de Details</a> -->
       </div>
     </div>
   </div>
   <div class="col-lg-4 col-md-6 mt-md-0 mt-5">	
    <div class="float-top">	
     <a href="services.php"><img src="assets/images/g22.jpg" class="img-responsive" height="200px" alt=""></a>
     <div class="float-lt feature-gd">	
       <h3><a href="services.php">Construction</a> </h3>
       <p> Consectetur adipisicingelit, sed do eiusmod tempor incididunt ut labore et.dolor sit amet </p>
       <!-- <a class="btn btn-secondary btn-theme4 mt-4" href="services.php"> plus de Details</a> -->
     </div>
   </div>
 </div>
 <div class="col-lg-4 col-md-6 mt-md-0 mt-5">	
  <div class="float-top">	
   <a href="services.php"><img src="assets/images/g222.jpg" class="img-responsive" height="200px" alt=""></a>
   <div class="float-lt feature-gd">	
     <h3><a href="services.php">Carrelage et Pavés</a> </h3>
     <p> Consectetur adipisicingelit, sed do eiusmod tempor incididunt ut labore et.dolor sit amet </p>
     <!-- <a class="btn btn-secondary btn-theme4 mt-4" href="services.php"> plus de Details</a> -->
   </div>
 </div>
</div>
<!-- 						<div class="col-lg-4 col-md-6 mt-lg-0 mt-5">	
							<div class="float-top-1 text-left pl-4 pr-4 py-5">	
								<h4 class="">Jobs</h4>
								<p class="mt-2"> Consectetur adipisicingelit, sed do eiusmod tempor incididunt ut labore et.dolor sit amet </p>
								<div class="top-right mt-4 pt-2">
									<div class="icon-left pt-1">
										<span class="fa fa-arrow-right"></span>
									</div>
									<div class="icon-text-left">
										<h3 class=""><a href="services.php">Metal Working Tools</a> </h3>
									<p class="mt-2"> Consectetur adipisicingelit, sed do eiusmod tempor incididunt ut labore et.dolor sit amet </p>
									</div>
								</div>
								<div class="top-right mt-3">
									<div class="icon-left pt-1">
										<span class="fa fa-arrow-right"></span>
									</div>
									<div class="icon-text-left">
										<h3 class=""><a href="services.php">Carrelage</a> </h3>
									<p class="mt-2"> Consectetur adipisicingelit, sed do eiusmod tempor incididunt ut labore et.dolor sit amet </p>
									</div>
								</div>
								</div>	
              </div> -->
              <div class="col-lg-4 col-md-6 mt-5">	
               <div class="float-top">	
                 <a href="services.php"><img src="assets/images/g333.jpg" class="img-responsive" alt="" height="200px" width="640px"></a>
                 <div class="float-lt feature-gd">	
                   <h3><a href="services.php">Placoplatre et peinture</a> </h3>
                   <p> Consectetur adipisicingelit, sed do eiusmod tempor incididunt ut labore et.dolor sit amet </p>
                   <!-- <a class="btn btn-secondary btn-theme4 mt-4" href="services.php">Plus de Details</a> -->
                 </div>
               </div>
             </div>
             <div class="col-lg-4 col-md-6 mt-5">
              <div class="float-top">		
               <a href="services.php"><img src="assets/images/g77.jpg" class="img-responsive" height="200px" alt=""></a>
               <div class="float-lt feature-gd">	
                 <h3><a href="services.php">Menuserie Aluminium</a> </h3>
                 <p> Consectetur adipisicingelit, sed do eiusmod tempor incididunt ut labore et.dolor sit amet </p>
                 <!-- <a class="btn btn-secondary btn-theme4 mt-4" href="services.php">Plus de Details</a> -->
               </div>
             </div>
           </div>
           <div class="col-lg-4 col-md-6 mt-5">	
             <div class="float-top">
              <a href="services.php"><img src="assets/images/g66.jpg" class="img-responsive" alt="" height="200px"></a>
              <div class="float-lt feature-gd">	
                <h3><a href="services.php">Menuserie Metalique</a> </h3>
                <p> Consectetur adipisicingelit, sed do eiusmod tempor incididunt ut labore et.dolor sit amet </p>
                <!-- <a class="btn btn-secondary btn-theme4 mt-4" href="services.php">Plus de Details</a> -->
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 mt-5">
            <div class="float-top">		
             <a href="services.php"><img src="assets/images/allu.jpg" class="img-responsive" height="200px" width="300px" alt=""></a>
             <div class="float-lt feature-gd">	
               <h3><a href="services.php">Allucobon et Vitrerie</a> </h3>
               <p> Consectetur adipisicingelit, sed do eiusmod tempor incididunt ut labore et.dolor sit amet </p>
               <!-- <a class="btn btn-secondary btn-theme4 mt-4" href="services.php">Plus de Details</a> -->
             </div>
           </div>
         </div>
         <div class="col-lg-4 col-md-6 mt-5">
          <div class="float-top">		
           <a href="services.php"><img src="assets/images/g81.jpeg" class="img-responsive" height="200px" alt=""></a>
           <div class="float-lt feature-gd">	
             <h3><a href="#">cablage et sécurite batiment</a> </h3>
             <p> Consectetur adipisicingelit, sed do eiusmod tempor incididunt ut labore et.dolor sit amet </p>
             <!-- <a class="btn btn-secondary btn-theme4 mt-4" href="services.php">Plus de Details</a> -->
           </div>
         </div>
       </div>
       <div class="col-lg-4 col-md-6 mt-5">
        <div class="float-top">		
         <a href="#"><img src="assets/images/entr.jpg" class="img-responsive" height="200px" alt=""></a>
         <div class="float-lt feature-gd">	
           <h3><a href="#">Rénovation  et entretien des édifices</a> </h3>
           <p> Consectetur adipisicingelit, sed do eiusmod tempor incididunt ut labore et.dolor sit amet </p>
           <!-- <a class="btn btn-secondary btn-theme4 mt-4" href="services.php">Plus de Details</a> -->
         </div>
       </div>
     </div>						 				 
   </div>  
 </div>
</div>
<!-- //features -->
</section>
<!-- customers4 block -->
<section class="w3l-customers-4" id="testimonials">
  <div id="customers4-block" class="py-5">
    <div class="container py-md-3">


      <div id="customerhnyCarousel" class="carousel slide" data-ride="carousel">

        <ol class="carousel-indicators">
          <li data-target="#customerhnyCarousel" data-slide-to="0" class="active"></li>
          <li data-target="#customerhnyCarousel" data-slide-to="1"></li>
          <li data-target="#customerhnyCarousel" data-slide-to="2"></li>
        </ol>
        <!-- Carousel items -->
        <div class="carousel-inner pb-5 mb-md-5 pt-md-5">

          <div class="carousel-item active">
            <div class="section-title">
              <div class="item-top">
                <div class="item text-center">
                  <div class="imgTitle">
                    <img src="assets/images/c1.jpg" class="img-responsive" alt="" />
                  </div>
                  <h6 class="mt-3">Steve Smith</h6>
                  <p class="">Client</p>
                  <h5>" Magna aliqua. Ut enim ad minim veniam, quis nostrud.Lorem ipsum dolor " </h5>

                </div>
              </div>
            </div>
          </div>
          <!--.item-->

          <div class="carousel-item">
            <div class="section-title">
              <div class="item-top">
                <div class="item text-center">
                  <div class="imgTitle">
                    <img src="assets/images/c2.jpg" class="img-responsive" alt="" />
                  </div>
                  <h6 class="mt-3">Jessey Rider</h6>
                  <p class="">Worker</p>
                  <h5>" Magna aliqua. Ut enim ad minim veniam, quis nostrud.Lorem ipsum dolor " </h5>

                </div>
              </div>
            </div>

          </div>
          <!--.item-->
          <div class="carousel-item">
            <div class="section-title">
              <div class="item-top">
                <div class="item text-center">
                  <div class="imgTitle">
                    <img src="assets/images/c3.jpg" class="img-responsive" alt="" />
                  </div>
                  <h6 class="mt-3">Mark Stoins</h6>
                  <p class="">Engineer</p>
                  <h5>" Magna aliqua. Ut enim ad minim veniam, quis nostrud.Lorem ipsum dolor " </h5>

                </div>
              </div>
            </div>
          </div>
          <!--.item-->

        </div>
        <!--.carousel-inner-->
      </div>




    </div>
  </div>


</section>
<!-- homepage blog grids -->
<span id="realisations"></span>
<section class="news-1" id="blog">
  <div class="news py-5">
    <div class="container py-md-3">
      <div class="heading text-center mx-auto">
        <h3 class="head">Quelques unes de nos realisations</h3>
        <p class="my-3 head"> Depuis sa creation, Maison Finie a realisé de nombreux projets parmis lesquels </p>
        
      </div>
      <div class="blog-grids row mt-5">
        <div class="col-lg-4 col-md-6 col-sm-12">
          <div class="blog-grid" id="zoomIn">
            <a href="#">
              <figure><img src="assets/images/g10.jpg" class="img-fluid" alt=""></figure>
            </a>
            <div class="blog-info">
              <h3><a href="#">Seven Outrageous Ideas Industry</a> </h3>
              <p> Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;
              Nulla mollis dapibus nunc</p>
              <ul>
                <li><a href="#author"><span class="fa fa-user-o mr-2"></span>Johnson smith</a></li>
                <li><a href="#author"><span class="fa fa-calendar mr-2"></span>Jan 16, 2020</a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-12 mt-md-0 mt-4">
          <div class="blog-grid" id="zoomIn">
            <a href="#">
              <figure height="400px"><img src="assets/images/44.jpg" class="img-fluid" alt="" ></figure>
            </a>
            <div class="blog-info">
              <h3><a href="#">Seven Doubts You Should Clarify About</a> </h3>
              <p> Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;
              Nulla mollis dapibus nunc</p>
              <ul>
                <li><a href="#author"><span class="fa fa-user-o mr-2"></span>Alexander</a></li>
                <li><a href="#author"><span class="fa fa-calendar mr-2"></span>Jan 16, 2020</a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-12 mt-lg-0 mt-4">
          <div class="blog-grid" id="zoomIn">
            <a href="#">
              <figure><img src="assets/images/g8.jpg" class="img-fluid" alt=""></figure>
            </a>
            <div class="blog-info">
              <h3><a href="#">Why You Should Not Go To Industry</a> </h3>
              <p> Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;
              Nulla mollis dapibus nunc</p>
              <ul>
                <li><a href="#author"><span class="fa fa-user-o mr-2"></span>Elizabeth ker</a></li>
                <li><a href="#author"><span class="fa fa-calendar mr-2"></span>Jan 16, 2020</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- //homepage blog grids -->


<!-- grids block 5 -->
<section class="w3l-footer-29-main" id="footer">
  <div class="footer-29">
    <div class="grids-forms-1 pb-5">
      <div class="container">
        <div class="grids-forms">
          <div class="main-midd">
            <h4 class="title-head">Newsletter – Restez informe de nos activites</h4>
            
          </div>
          <div class="main-midd-2">
            <form action="#" method="post" class="rightside-form">
              <input type="email" name="email" placeholder="Entrrez votre email">
              <button class="btn" type="submit">S'inscrire</button>
            </form>
          </div>
        </div>
      </div>
    </div>
    <div class="container pt-5">
      
      <div class="d-grid grid-col-4 footer-top-29">
        <div class="footer-list-29 footer-1">
          <h6 class="footer-title-29">A propos de nous</h6>
          <ul>
           <p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae</p>
         </ul>
         <div class="main-social-footer-29">
          <h6 class="footer-title-29">Reseaux sociaux</h6>
          <a href="#facebook" class="facebook"><span class="fa fa-facebook"></span></a>
          <a href="#twitter" class="twitter"><span class="fa fa-twitter"></span></a>
          <a href="#instagram" class="instagram"><span class="fa fa-instagram"></span></a>
          <a href="#google-plus" class="google-plus"><span class="fa fa-google-plus"></span></a>
          <a href="#linkedin" class="linkedin"><span class="fa fa-linkedin"></span></a>
        </div>
      </div>
      <div class="footer-list-29 footer-2" >
        <ul>
          <h6 class="footer-title-29">Liens utiles</h6>
          <li><a href="contact.php">Privacy Policy</a></li>
          <li><a href="contact.php">Help Desk</a></li>
          <li><a href="services.php">Projects</a></li>
          <li><a href="contact.php">All Users</a></li>
          <li><a href="contact.php">Support</a></li>
        </ul>
      </div>
      <div class="footer-list-29 footer-3" >
        <div class="properties">
          <h6 class="footer-title-29">Realisations Recentes</h6>
          <a href="#"><img src="assets/images/g2.jpg" class="img-responsive" alt=""><p>We Are Leading International Consultiing Agency</p></a>
          <a href="#"><img src="assets/images/g8.jpg" class="img-responsive" alt=""><p>Digital Marketing Agency all the foundational tools</p></a>
          <a href="#"><img src="assets/images/g6.jpg" class="img-responsive" alt=""><p>Doloremque velit sapien labore eius itna</p></a>
        </div>
      </div>
      <div class="footer-list-29 footer-4">
        <ul>
          <h6 class="footer-title-29">Liens Rapides</h6>
          <li><a href="index.php">Acceuil</a></li>
          <li><a href="#Apropos">A propos</a></li>
          <li><a href="#services">Services</a></li>
          <li><a href="#realisations">Realisation</a></li>
          <li><a href="contact.php">Contact</a></li>
        </ul>
      </div>
    </div>
    <div class="bottom-copies text-center">
      <p class="copy-footer-29">© 2021 <span style="color: #4682b4;">Maison </span> <span style="color:#cd5c5c;">Finie</span>
      </a> . All rights reserved | Designed by <a href="https://w3layouts.com">sublimecamer</a></p>
      
    </div>
  </div>
</div>
<!-- move top -->
<button onclick="topFunction()" id="movetop" title="Go to top">
  <span class="fa fa-angle-up"></span>
</button>
<script>
                     // When the user scrolls down 20px from the top of the document, show the button
                     window.onscroll = function () {
                       scrollFunction()
                     };
                     
                     function scrollFunction() {
                       if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                         document.getElementById("movetop").style.display = "block";
                       } else {
                         document.getElementById("movetop").style.display = "none";
                       }
                     }
                     
                     // When the user clicks on the button, scroll to the top of the document
                     function topFunction() {
                       document.body.scrollTop = 0;
                       document.documentElement.scrollTop = 0;
                     }
                   </script>
                   <!-- /move top -->
                 </section>
                 <!-- // grids block 5 -->
                 <!-- <script src="assets/js/jquery-3.3.1.min.js"></script> -->
                 <!-- //footer-28 block -->
               </section>

               <script>
                $(function () {
                  $('.navbar-toggler').click(function () {
                    $('body').toggleClass('noscroll');
                  })
                });
              </script>
              <!-- jQuery first, then Popper.js, then Bootstrap JS -->
              <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
              integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
            </script>
            
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
            integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
          </script>

          <!-- Smooth scrolling -->

          <script src="assets/js/owl.carousel.js"></script>

          <!-- script for -->




          <script>
            $(document).ready(function () {
              $('.owl-one').owlCarousel({
                loop: true,
                margin: 0,
                nav: true,
                responsiveClass: true,
                autoplay: false,
                autoplayTimeout: 5000,
                autoplaySpeed: 1000,
                autoplayHoverPause: false,
                responsive: {
                  0: {
                    items: 1,
                    nav: false
                  },
                  480: {
                    items: 1,
                    nav: false
                  },
                  667: {
                    items: 1,
                    nav: true
                  },
                  1000: {
                    items: 1,
                    nav: true
                  }
                }
              })
            })
          </script>






          <!-- //script -->
          <script src="assets/js/smartphoto.js"></script>
          <script>
            document.addEventListener('DOMContentLoaded', function () {
              const sm = new SmartPhoto(".js-img-viwer", {
                showAnimation: false
              });
    // sm.destroy();
  });
</script>
<script>
  $(document).ready(function () {
    $('#modalBtn').onClick(function(){
      alert('salut')
    })    
    
    // var btn = document.getElementById('modalBtn')
    // var modal = document.getElementById('ModalDevis')

    // btn.addEventlistener('click', function(e){
      
    //   // modal.classList.remove('modal-backdrop')
    // })
  }
</script>

<script>
  $(document).ready(function(){
    
    $("#modalBtn").click(function(){
      $("#ModalDevis").modal({backdrop: false});
    });
    
  });
</script>
</body>

</html>
<!-- // grids block 5 -->

